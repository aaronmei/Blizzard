package com.suez.nullpointerexception.blizzard.client;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ClientReaderRunnable implements Runnable{
	private Socket socket;
	private ObjectInputStream ois;
	private String type;
	private String msg;
	private List<Object> list =new ArrayList<>();
	
	
	public ClientReaderRunnable(Socket socket)
	{
	this.socket = socket; 
	}
	
	//@Override
	@SuppressWarnings("unchecked")
	public void run() {
		// TODO Auto-generated method stub
		
		while(true){
			
			try {
				ois = new ObjectInputStream(socket.getInputStream());
				type = ois.readObject().toString();
				switch (type) {
					case "bool":
						msg = ois.readObject().toString();
						if("true\n".equals(msg)){System.out.println("Instruction execute successfully!");continue;}
						if("false\n".equals(msg)){System.out.println("Instruction execute failure! No data update!");continue;}
						if("leave\n".equals(msg)){System.out.println("You leaved Blizzard System...\n");System.exit(0);}
						break;
		
					case "list":
						list = (List<Object>) ois.readObject();
						for(int i = 0 ;i<list.size();i++)
						{
							System.out.println(list.get(i).toString());
						}
						break;
					case "off":
						System.out.println("Blizzard System is off,please try later...\n");System.exit(0);
						break;
					case "error":
						System.out.println("Input Error!");System.exit(0);
						break;
				}
				} catch (IOException | ClassNotFoundException e) {
				// TODO Auto-generated catch block
					e.printStackTrace();
				//System.out.println("Blizzard Server is off!");System.exit(0);
				}
		}
	}
}