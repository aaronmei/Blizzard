package com.suez.nullpointerexception.blizzard.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class CClient {
	
	static Socket socket;
	public static void main(String[] args) throws UnknownHostException, IOException {
		try {
			socket = new Socket("127.0.0.1",5555);
		} catch (Exception e) {
			System.out.println("Blizzard Server is off!\nPlease try later...");
			System.exit(0);
		}
		
		InputStream is = socket.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		
		String logMsg = reader.readLine();//read the logMsg
		
		try{
			if("on".equals(logMsg)){
				System.out.println("----------Welcome to Blizzard System!----------");
				new Thread(new ClientReaderRunnable(socket)).start();
				while(!socket.isClosed())
				{
					instr(socket);
				}
				System.out.println("Blizzard Server is off!\nPlease try later...");System.exit(0);
			}		

			else{
				System.out.println("Blizzard Server is off!\nPlease try later...");System.exit(0);
			}
		}catch(Exception e){
			System.out.println("Blizzard Server is off!\nPlease try later...");System.exit(0);
		}
		
	}
	
	private static void instr(Socket socket) throws IOException{
		System.out.println("Please input the instruction:");
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		String msg = scanner.nextLine();
		//System.out.println(msg);
		OutputStream os = socket.getOutputStream();
		os.write((msg+"\n").getBytes("utf-8"));
		//scanner.close();	
	}
}
