package com.suez.nullpointerexception.blizzard.dao;

import java.util.List;

import com.suez.nullpointerexception.blizzard.entity.Person;

public interface PersonDao {
	public List<Person> getAll();
	public boolean addPerson(Person p);
	public boolean updatePerson(Person p);
	public boolean deletePerson (Person p);
}
