package com.suez.nullpointerexception.blizzard.global;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.suez.nullpointerexception.blizzard.controller.AController;
import com.suez.nullpointerexception.blizzard.entity.Dept;
import com.suez.nullpointerexception.blizzard.entity.Person;


public class Global {
	public static List<Socket> socketPool = new ArrayList<Socket>();
	public static Map<String, AController> controllers = new HashMap<>();
	public static Map<String,Date> userMsg = new HashMap<>();
	public static Date serDate;
	public static String logMsg = "off";//on or off
	
	public static Map<String ,String> analys(Socket socket,String protocol){
		Map<String, String>  map = new HashMap<>();
		String tabName = new String();
		try{
			String[] temp = protocol.split(" ");
			
			String action = temp[0].split("-")[0];
			map.put("action",action);
			String name = temp[0].split("-")[1].toLowerCase();
			
			switch(action+name){
				case "Ap": tabName = "Person";break;
				case "Dp": tabName = "Person";break;
				case "Up": tabName = "Person";break;
				case "Ad": tabName = "Dept";break;
				case "Dd": tabName = "Dept";break;
				case "Ud": tabName = "Dept";break;
				case "Lpa": tabName = "Person";break;
				case "Lda": tabName = "Dept";break;
				case "Lpd": tabName = "Person";map.put("action2", "d");break; 
				default:
				try {
					ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
					oos.writeObject("error");
					System.out.println("Client Input Error!");
					Thread.currentThread().stop();
					Global.socketPool.remove(socket);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}		
			map.put("tabName",tabName);		
			String[] temp1 = temp[1].split(",");
			for(String xx: temp1){
			map.put(xx.split(":")[0],xx.split(":")[1]);
					
				
				}
		}catch(ArrayIndexOutOfBoundsException e){
			//System.err.println("Error!");
		}
		return map;
	}
	
	public static Person createPerson(Map<String,String> map){
		Set<String> set = map.keySet();
		Person person = new Person();
		for(String xx : set){
			switch (xx) {
			case "pid":
				person.setPid(Integer.parseInt(map.get(xx)));
				break;

			case "birthday":
				//SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd");
				person.setBirthday(java.sql.Date.valueOf(map.get(xx)));
					//person.setBirthday(new java.sql.Date(date.getDate()));
				
				break;
			case "name":
				person.setName(map.get(xx));
				break;
			case "tel":
				person.setTel(map.get(xx));
				break;
			case "did":
				person.setDid(Integer.parseInt(map.get(xx)));
				break;
			case "salary":
				person.setSalary(Double.parseDouble(map.get(xx)));
				break;
			}
		}
		return person;
	}
	
	public static Dept createDept(Map<String,String> map){
		Set<String> set = map.keySet();
		Dept dept = new Dept(); 
		for(String xx : set){
			switch (xx) {
			case "did":
				dept.setDid(Integer.parseInt(map.get(xx)));
				break;
			case "name":
				dept.setName(map.get(xx));
				break;
//			case "slary":
//				dept.setSlary(Double.parseDouble(map.get(xx)));
//				break;
			case "city":
				dept.setCity(map.get(xx));
				break;
			}					
		}			
		return dept;
	}
	/*
	public static void main(String[] args) {
		Person p = new Person();
		String s = "1993-10-01";
		p.setBirthday(java.sql.Date.valueOf(s));
		System.out.println(p.getBirthday());
	}*/
}
