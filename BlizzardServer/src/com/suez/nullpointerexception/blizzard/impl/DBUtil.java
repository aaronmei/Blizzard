package com.suez.nullpointerexception.blizzard.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBUtil {
	
	private static String className = "oracle.jdbc.OracleDriver";
	private static String url = "jdbc:oracle:thin:@ITA-031-W7:1521:XE";
	private static String user = "kieren";
	private static String password = "kieren";
	
	static{
		try{
			Class.forName(className);
		}catch(ClassNotFoundException e){
			e.printStackTrace();
		}
	}
	
	public static Connection getConnection(){
		Connection conn = null;
		try {
		
			conn = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	public static void free(Connection conn,PreparedStatement ps,ResultSet rs){
			try {
				if(rs!=null)   		rs.close();
				if(ps!=null)   		ps.close();
				if(conn!=null)		conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}
	
	
}
