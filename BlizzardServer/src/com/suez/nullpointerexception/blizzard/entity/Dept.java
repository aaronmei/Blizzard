package com.suez.nullpointerexception.blizzard.entity;

import java.io.Serializable;

public class Dept implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int getDid() {
		return did;
	}
	public void setDid(int did) {
		this.did = did;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	private int did;
	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	private String city;
	@Override
	public String toString() {
		return "Dept [did=" + did + ", name=" + name + ", city=" + city + "]";
	}
	
	/*
	public static void main(String[] args) {
		Dept dept = new Dept();
		dept.setCity("zhuhai");
		dept.setDid(222);
		dept.setSlary(1922);
		System.err.println(dept.toString());
	}
	
	*/
}
