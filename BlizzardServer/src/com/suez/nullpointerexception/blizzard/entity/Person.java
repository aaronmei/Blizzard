package com.suez.nullpointerexception.blizzard.entity;

import java.io.Serializable;
import java.sql.Date;

public class Person implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int pid;
	private String name;
	private double salary;
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	private Date birthday;
	private String tel;
	private int did;
	public int getDid() {
		return did;
	}
	public void setDid(int did) {
		this.did = did;
	}
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	
	public Date getBirthday() {
		if(birthday==null) return java.sql.Date.valueOf("1900-01-01");
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	@Override
	public String toString() {
		return "Person [pid=" + pid + ", name=" + name + ", salary=" + salary
				+ ", birthday=" + birthday + ", tel=" + tel + ", did=" + did
				+ "]";
	}
	
	
	/*
	public static void main(String[] args) {
		Person person = new Person();
		//person.setBirthday(java.sql.Date.valueOf(null));
		System.out.println(person.getBirthday().toString());
	}
	*/
}


