

package com.suez.nullpointerexception.blizzard.bserver;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.suez.nullpointerexception.blizzard.cserver.CServer;

public class HttpServer {
	
	public static CServer TCPServer = new CServer();
	
	public static void main(String[] args) throws IOException {

		@SuppressWarnings("resource")

		ServerSocket server = new ServerSocket(8080);
		ExecutorService executor = Executors.newCachedThreadPool();
		try{
			while(true){
				Socket socket = server.accept();
				executor.submit(new Router(socket));
			}
		}catch(Exception e)
		{
			System.out.println("Blizzard Server is off!");
		}
	}

	
}

