
package com.suez.nullpointerexception.blizzard.bserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

import com.suez.nullpointerexception.blizzard.controller.RouterHomeController;
import com.suez.nullpointerexception.blizzard.controller.RouterStartController;
import com.suez.nullpointerexception.blizzard.controller.RouterStopController;

public class Router implements Runnable{
	private Socket socket;
	public Router(Socket socket){
		this.socket = socket;
	}
	
	@Override
	public void run() {
		try {
			OutputStream os = socket.getOutputStream();
			InputStream is = socket.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String str = reader.readLine();
			
			if(str==null){
				is.close();
				os.close();return;
			}
			if(str.contains("start")){
				new RouterStartController().execute(socket.getInputStream(),socket.getOutputStream());
			}else if(str.contains("stop")){
				new RouterStopController().execute(socket.getInputStream(),socket.getOutputStream());
			}else if(!str.contains(".ico")){
				new RouterHomeController().execute(socket.getInputStream(), socket.getOutputStream());
			}
			
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}  
	}
}
