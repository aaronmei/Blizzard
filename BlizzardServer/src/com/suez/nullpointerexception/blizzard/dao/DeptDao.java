package com.suez.nullpointerexception.blizzard.dao;

import java.util.List;

import com.suez.nullpointerexception.blizzard.entity.Dept;
import com.suez.nullpointerexception.blizzard.entity.Person;

public interface DeptDao {
	List<Dept> getAll();
	boolean insert(Dept d);
	boolean update(Dept d);
	boolean delete(Dept d);
	List<Person> getAllPersonById(int did);
}
