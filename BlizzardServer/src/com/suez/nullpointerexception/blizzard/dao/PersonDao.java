package com.suez.nullpointerexception.blizzard.dao;

import java.util.List;

import com.suez.nullpointerexception.blizzard.entity.Person;

public interface PersonDao {
	List<Person> getAll();
	boolean insert(Person p);
	boolean update(Person p);
	boolean delete(Person p);
}
