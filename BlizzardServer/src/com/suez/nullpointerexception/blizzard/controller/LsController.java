package com.suez.nullpointerexception.blizzard.controller;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.suez.nullpointerexception.blizzard.entity.Dept;
import com.suez.nullpointerexception.blizzard.entity.Person;
import com.suez.nullpointerexception.blizzard.global.Global;
import com.suez.nullpointerexception.blizzard.impl.DeptDaoImpl;
import com.suez.nullpointerexception.blizzard.impl.PersonDaoImpl;

public class LsController implements AController {
	private Map<String,String> map;
	private List<Person> list1 = new ArrayList<>();
	private List<Dept> list2 = new ArrayList<>();
	private ObjectOutputStream oos;
	@Override
	public void execute(Socket socket, String protocol) throws IOException {
		// TODO Auto-generated method stub
		map = Global.analys(socket ,protocol);
		String tabName = map.get("tabName");
		String action2 = map.get("action2");
		oos = new ObjectOutputStream(socket.getOutputStream());
		switch (tabName) {
		case "Person":
			if("d".equals(action2)){
				list1 = new DeptDaoImpl().getAllPersonById(Integer.parseInt(map.get("did")));
				System.out.println(list1);
				if(list1.size()==0){
					oos.writeObject("false\n");
				}
				else{
					oos.writeObject("list");
					oos.writeObject(list1);
				}
			}
			else{
				list1 = new PersonDaoImpl().getAll();
				System.out.println(list1);
				if(list1.size()==0){
					oos.writeObject("false\n");
				}
				else{
					oos.writeObject("list");
					oos.writeObject(list1);
				}
			}			
			break;

		case "Dept":
			list2 = new DeptDaoImpl().getAll();
			if(list2.size()==0){
				oos.writeObject("false\n");
			}
			else{
				oos.writeObject("list");
				oos.writeObject(list2);
			}
			break;
			
		}	
	}

}


