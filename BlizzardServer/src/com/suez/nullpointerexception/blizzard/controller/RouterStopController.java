
package com.suez.nullpointerexception.blizzard.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import com.suez.nullpointerexception.blizzard.bserver.HttpServer;
import com.suez.nullpointerexception.blizzard.global.Global;

public class RouterStopController implements RouterControllerInter{

	@Override
	public void execute(InputStream is,OutputStream os) throws IOException {
		// TODO Auto-generated method stub
		synchronized (Global.logMsg) {
			if("on".equals(Global.logMsg)){
				Global.logMsg = "off";
				HttpServer.TCPServer.stop();
				System.out.println("TCPServer stop");
			}
			
		}
		PrintStream out = new PrintStream(os);
		String html = "<!DOCTYPE html><html><head><title>home</title></head><body><form action=\"/start\" method=\"POST\"><input type=\"submit\" value=\"start\"> </form> </body></html>";  
		out.println(html); 
	    out.flush();
	    out.close();
	    is.close();
	    //System.out.println("close");   
	}

}

