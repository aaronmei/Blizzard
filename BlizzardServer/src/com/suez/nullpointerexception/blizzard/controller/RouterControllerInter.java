package com.suez.nullpointerexception.blizzard.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface RouterControllerInter {
	void execute (InputStream is,OutputStream os) throws IOException;
}
