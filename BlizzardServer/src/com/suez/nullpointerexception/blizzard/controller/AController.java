package com.suez.nullpointerexception.blizzard.controller;

import java.io.IOException;
import java.net.Socket;

public interface AController {
	void execute(Socket socket ,String protocol) throws IOException;
}
