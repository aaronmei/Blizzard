
package com.suez.nullpointerexception.blizzard.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.suez.nullpointerexception.blizzard.bserver.HttpServer;
import com.suez.nullpointerexception.blizzard.global.Global;

public class RouterStartController implements RouterControllerInter{
	private String date;
	@Override
	public void execute(InputStream is ,OutputStream os) throws IOException {
		synchronized (Global.logMsg) {
			if("off".equals(Global.logMsg)){
				Global.logMsg = "on";
				new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							HttpServer.TCPServer.start();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}).start();
			}
			
		}
	
		PrintStream out = new PrintStream(os);
		//StringBuilder html = new StringBuilder("<!DOCTYPE html><meta http-equiv=\"refresh\" content=\"1\"><html><head><link href=\"beauty.css\" rel=\"stylesheet\" type=\"text/css\"/><title>home</title></head><body><form action=\"/stop\" method=\"POST\"><input type=\"submit\" value=\"stop\"> </form>");
		StringBuilder html = new StringBuilder("<!DOCTYPE html><meta http-equiv=\"refresh\" content=\"1\"><html><head><title>home</title></head><body><form action=\"/stop\" method=\"POST\"><input type=\"submit\" value=\"stop\"> </form>");
		html.append("<br>");
		html.append("<table border=\"1\">");
		Map<String,Date> userInfo = Global.userMsg;
		//System.out.println(userInfo);
		
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//MM是月份，mm是分钟；
		
		html.append("<tr>");
		html.append("<th>").append("-").append("</th>");
		html.append("<th>").append("IP Adress").append("</th>");
		html.append("<th>").append("Time").append("</th>");
		html.append("</tr>");
		
		html.append("<tr>");
		html.append("<th>").append("Blizzard Sever start time").append("</th>");
		html.append("<th>").append("<>").append("</th>");
		html.append("<th>").append("<").append(format.format(Global.serDate)).append(">").append("</th>");
		html.append("</tr>");
		for(String key:Global.userMsg.keySet()){
			html.append("<tr>");
			html.append("<th>").append("Client login").append("</th>");
			html.append("<th>").append("<").append(key).append(">").append("</th>");
			date = format.format(userInfo.get(key));
			//System.out.println(date);
			html.append("<th>").append("<").append(date).append(">").append("</th>");
			html.append("</tr>");
		}
		html.append("</table>");
		html.append("</body></html>");
		out.println(html); 
	    out.flush();
	    out.close();
	    
	}
}


