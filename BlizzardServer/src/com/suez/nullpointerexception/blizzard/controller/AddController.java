package com.suez.nullpointerexception.blizzard.controller;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import com.suez.nullpointerexception.blizzard.entity.Dept;
import com.suez.nullpointerexception.blizzard.entity.Person;
import com.suez.nullpointerexception.blizzard.global.Global;
import com.suez.nullpointerexception.blizzard.impl.DeptDaoImpl;
import com.suez.nullpointerexception.blizzard.impl.PersonDaoImpl;

public class AddController implements AController {
	private Map<String,String> map= new HashMap<String, String>();
	private ObjectOutputStream oos;
	private Person person;
	private Dept dept;
	Boolean f1;
	@Override
	public void execute(Socket socket, String protocol) throws IOException {
		// TODO Auto-generated method stub
		map = Global.analys(socket,protocol);
		String tabName = map.get("tabName");
		switch (tabName) {
		case "Person":
			person = Global.createPerson(map);
			f1 = new PersonDaoImpl().insert(person); 
			break;

		case "Dept":
			dept = Global.createDept(map);	
			f1 = new DeptDaoImpl().insert(dept);
			break;
			
		}
		oos = new ObjectOutputStream(socket.getOutputStream());
		if(f1){
			oos.writeObject("bool");
			oos.writeObject("true\n");
		}
		else{
			oos.writeObject("bool");
			oos.writeObject("false\n");
		}
	}

}
