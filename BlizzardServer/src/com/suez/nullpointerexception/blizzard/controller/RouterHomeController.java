package com.suez.nullpointerexception.blizzard.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.suez.nullpointerexception.blizzard.global.Global;

public class RouterHomeController implements RouterControllerInter {
	
	@Override
	public void execute(InputStream is,OutputStream os) throws IOException {
		synchronized (Global.logMsg) {
			if(Global.logMsg.equals("off")){
				new RouterStopController().execute(is,os);
				
			}else{
				System.out.println("错误页面");
			}
		}
		
	}
	

}
